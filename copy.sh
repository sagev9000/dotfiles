#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
FAKE_HOME="$SCRIPT_DIR/fake_home"

git submodule init
git submodule update

dotdirs="$(find $FAKE_HOME -mindepth 1 -type d)"

echo "$dotdirs" | while read -r dotdir; do
  new="$HOME${dotdir:${#FAKE_HOME}}"
  echo "mkdir -p $new"
  mkdir -p $new
done


dotfiles="$(find $FAKE_HOME -mindepth 1 -type f)"

echo "$dotfiles" | while read -r dotfile; do
  new="$HOME${dotfile:${#FAKE_HOME}}"
  echo "$dotfile => $new"
  ln -sf "$dotfile" "$new"
done

touch "$HOME/.zsh_local"
if [[ "$1" == "--files-only" ]]; then
  exit 0
fi

echo
echo "Installing required packages..."

echo -n "Rust is "
if ! command -v cargo &> /dev/null; then
  echo "not installed. Installing..."
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
else
  echo "already installed."
fi

. "$HOME/.cargo/env"
if command -v cargo &> /dev/null && ! command -v bat &> /dev/null; then
  cargo install bat
fi

echo -n "nvm is "
if ! test -d "$HOME/.nvm"; then
  echo "not installed. Installing..."
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
  nvm install 14
  nvm install 18
  nvm install node
  nvm use node
else
  echo "already installed."
fi

echo -n "dotnet is "
if ! command -v dotnet &> /dev/null; then
  echo "not installed. Installing..."
  wget https://dot.net/v1/dotnet-install.sh -O /tmp/dotnet-install.sh
  chmod +x /tmp/dotnet-install.sh
  /tmp/dotnet-install.sh --version latest
  rm /tmp/dotnet-install.sh
else
  echo "already installed."
fi

echo -n "starship is "
if ! command -v starship &> /dev/null; then
  echo "not installed. Installing..."
curl -sS https://starship.rs/install.sh | sh
echo "Starship requires an NFont: https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip"
else
  echo "already installed."
fi

echo -n "JetBrains toolbox is "
if ! test -d $HOME/.config/JetBrains/; then
  echo "not installed. Installing..."
  ARCHIVE_URL="$(curl -s 'https://data.services.jetbrains.com/products/releases?code=TBA&latest=true&type=release' | grep -Po '"linux":.*?[^\\]",' | awk -F ':' '{print $3,":"$4}'| sed 's/[", ]//g')"
  wget -q --show-progress -cO "/tmp/jetbrains-toolbox-latest.tar.gz" "$ARCHIVE_URL"
  cd /tmp/
  tar -xvf jetbrains-toolbox-latest.tar.gz --strip-components=1
  chmod +x jetbrains-toolbox
  ./jetbrains-toolbox
else
  echo "already installed."
fi

if ! command -v fzf &> /dev/null; then
  echo "You may wish to install fzf for sofi and other search"
else
  echo "fzf is already installed"
fi
