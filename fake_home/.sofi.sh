#!/bin/bash

WINDOW_NAME="active-sofi-window"
CACHE_FILE="$HOME/.app-cache-sofi"

IGNORED_APPS="Firetools|cmatrix"
APP_LOCATIONS="/usr/share/applications $HOME/.local/share/applications "
FILE_LOCATIONS="$HOME/Documents $HOME/Downloads"

! [[ "$XDG_SESSION_TYPE" = "x11" ]]
IS_X11="$?"

sofi_id() {
     gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/Windows --method org.gnome.Shell.Extensions.Windows.List | cut -c 3- | rev | cut -c4- | rev | jq '.[] | .id' | while read id; do
       if gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/Windows --method org.gnome.Shell.Extensions.Windows.GetTitle $id | grep "SOFI" &> /dev/null; then
           echo $id
       fi
     done
}

# Open search window or raise existing one
if [[ "$1" == "--launch" ]]; then
  if (($IS_X11)) && existing="$(xdotool search $WINDOW_NAME)"; then
    xdotool windowraise "$existing"
  else
    # xfce4-terminal --role="$WINDOW_NAME" --title="$WINDOW_NAME" --startup-id="$WINDOW_NAME" -e "bash -c '$HOME/.sofi.sh'"

    
    id="$(sofi_id)"
    if [[ "$id" != "" ]]; then
      gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/Windows --method org.gnome.Shell.Extensions.Windows.Activate $id
    else
        xterm -geometry 120x40 -fa 'Monospaced' -fs 10 -T "SOFI" -e "bash -c '$HOME/.sofi.sh'"
    fi
    # st -e bash -c '$HOME/.sofi.sh'"
  fi
  exit $?
fi

windows() {
  if ! (($IS_X11)); then
    gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/Windows --method org.gnome.Shell.Extensions.Windows.List | cut -c 3- | rev | cut -c4- | rev | jq '.[] | .id' | while read id; do
      echo "win" "$(gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/Windows --method org.gnome.Shell.Extensions.Windows.GetTitle $id | cut -c 3- | rev | cut -c4- | rev)" "|" $id
    done
  else
    xprop -root _NET_CLIENT_LIST |
      pcregrep -o1 '# (.*)' |
      sed 's/, /\n/g' |
      xargs -I {} -n1 xprop -id {} _NET_WM_NAME |
      grep -v '"Desktop"\|"xfce4-panel"' |
      sed 's/_NET_WM_NAME(UTF8_STRING) = "/win /' | sed 's/"$//'
  fi
}

read_apps() {
  cat "$CACHE_FILE"
  # shellcheck disable=SC2086
  nohup find $APP_LOCATIONS -name '*.desktop' -exec grep '^Name=' {} \; |
    egrep -iv "$IGNORED_APPS" |
    sed 's/Name=/app /' > "$CACHE_FILE" &
}

read_snap_apps() {
    snap list | tail -n +2 | awk '{print "snp " $1}'
}

open_app() {
  app="$(echo "$1" | cut -c 5-)"
  # shellcheck disable=SC2086
  find $APP_LOCATIONS -name '*.desktop' | while read -r f; do
    if grep "Name=$app" "$f" &> /dev/null; then
      setsid exo-open "$f" >/dev/null 2>&1 </dev/null &
      return
    fi
  done
}

list_files() {
  find $FILE_LOCATIONS -maxdepth 1 -not -name '.*' |
    sed "s@$HOME@\~@" |
    sed 's/^/fil /'
}

paste_emoji() {
  emoji="$(echo -n "$1" | cut -c 5- | sed 's/ .*//')"
  nohup xclip -selection clipboard <(echo -n $emoji) &> /dev/null &
}

if [[ "$1" == "--print-only" ]]; then
    cat \
      <(windows) \
      <($HOME/.rofi-list-git-repos.py --no-icon) \
      <(read_apps) \
      <(read_snap_apps) \
      <(list_files) \
      $HOME/.emojis
    exit 1
fi

choice="$(cat \
  <(windows) \
  <("$HOME/.rofi-list-git-repos.py") \
  <(read_apps) \
  <(read_snap_apps) \
  <(list_files) \
  "$HOME/.emojis" \
  | fzf --print-query --preview="$HOME/.sofi-preview.sh {} {q}")"
echo
qstring="$(echo "$choice" | head -n 1)"
choice="$(echo "$choice" | tail -n 1)"

mvn_handler() {
    qarg="$(echo "$1" | cut -c 5-)"
    row_count=10
    arr_result="$(curl -s "https://search.maven.org/solrsearch/select?q=$qarg&rows=$row_count&wt=json" | jq '[.response.docs[] | { g, a, latestVersion } ]')"

    for i in 0 1 2 3 4 5 6 7 8 9; do
        groupId="$(echo -n "$arr_result" | jq -r ".[$i].g")"
        artifactId="$(echo -n "$arr_result" | jq -r ".[$i].a")"
        latestVersion="$(echo -n "$arr_result" | jq -r ".[$i].latestVersion")"
        echo "mvr $groupId:$artifactId $latestVersion"
    done | fzf --print-query --preview="$HOME/.sofi-preview.sh {} {q}"
}

npm_handler() {
    qarg="$(echo "$1" | cut -c 5-)"
    NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    package="$(npm search $qarg | tail -n +2 | fzf | sed 's/ .*//')"
    setsid xdg-open "https://npmjs.com/package/$package" &> /dev/null &
}

case $choice in
  git*)
    setsid "$HOME/.rofi-list-git-repos.py" "$choice" &> /dev/null &
    ;;
  win*)
    if (($IS_X11)); then
      wmctrl -i -R "$(xwininfo -root -tree |
        grep "$(echo "$choice" | cut -c 5-)" |
        awk '{ print $1 }' |
        sed 's/0x/0x00/')"
    else
      gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/Windows --method org.gnome.Shell.Extensions.Windows.Activate "$(echo "$choice" | sed 's/.* \([0-9]\+\)$/\1/')"
    fi
    ;;
  app*)
    open_app "$choice"
    ;;
  snp*)
    app="$(echo "$choice" | cut -c 5-)"
    setsid $app >/dev/null 2>&1 </dev/null &
    ;;
  emj*)
    paste_emoji "$choice"
    ;;
  fil*)
    setsid xdg-open "$(echo $choice | cut -c 5- | sed "s@~@$HOME@")" &> /dev/null &
    ;;

  *)
    case $qstring in
      mvn*)
        lib="$(mvn_handler "$qstring")"
        lib="$(echo "$lib" | cut -c 5-)"
        groupId="$(echo -n "${lib//[$'\t\r\n']}" | sed 's/\(.*\):.*/\1/')"
        artifactId="$(echo -n "${lib//[$'\t\r\n']}" | sed 's/.*:\(.*\) .*/\1/')"
        latestVersion="$(echo -n "${lib//[$'\t\r\n']}" | sed 's/.* \(.*\)/\1/')"

        xml="$(cat <<EOF
<dependency>
    <groupId>${groupId}</groupId>
    <artifactId>${artifactId}</artifactId>
    <version>${latestVersion}</version>
</dependency>
EOF
)"
        if (($IS_X11)); then
            nohup xclip -selection clipboard <(echo -n "$xml") &> /dev/null &
        else
            wl-copy "$xml"
        fi
        echo "Copied to clipboard!"
        sleep 0.5
        ;;

      npm*)
        lib="$(npm_handler "$qstring")"
        lib="$(echo "$lib" | cut -c 5-)"
        ;;

    esac
    ;;
esac

sleep 0.1
