#!/bin/bash

FIREFOX_PROFILE="$HOME/.mozilla/firefox/7sxxu758.dev-edition-default"
PLACES_FILE="$FIREFOX_PROFILE/places.sqlite"
WORKING_PLACES="$FIREFOX_PROFILE/places2.sqlite"
CACHE_FILE="$HOME/.rofi-history-cache"

function duplicate_history_file() {
  cp $PLACES_FILE $WORKING_PLACES
}

if [[ "$1" == "" ]]; then
  if [[ "1" == "$(( (`date +%s` - `stat -L --format %Y $CACHE_FILE.sqlite`) > (30*60) ))" ]]; then
    duplicate_history_file
    sqlite3 "$WORKING_PLACES" "SELECT datetime(a.visit_date/1000000,'unixepoch') AS visit_date, b.title, b.url FROM moz_historyvisits AS a JOIN moz_places AS b ON a.place_id=b.id WHERE 1 ORDER BY a.visit_date DESC LIMIT 10000;" > "$CACHE_FILE"
  fi
  cat "$CACHE_FILE"
fi

if [[ "$1" != "" ]]
  firefox-beta-bin "$(grep "$1" "$CACHE_FILE" | head -n 1 | sed 's/.*|//g')"
fi
