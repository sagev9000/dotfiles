#!/bin/bash

if [[ "$1" == "" ]]; then
  bt list
  exit 0
fi

bt activate --focused $(echo "$@" | awk '{print $1}') &> /dev/null
