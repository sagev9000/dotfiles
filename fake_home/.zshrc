if test -f /usr/share/doc/fzf/examples/completion.zsh &> /dev/null; then
    source /usr/share/doc/fzf/examples/completion.zsh
fi

HISTFILE=~/.histfile
HISTSIZE=SAVEHIST=100000
setopt appendhistory autocd notify
setopt menu_complete
unsetopt beep
bindkey -v

autoload -Uz up-line-or-beginning-search
autoload -Uz down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey '\eOA' up-line-or-beginning-search
bindkey '\e[A' up-line-or-beginning-search
bindkey '\eOB' down-line-or-beginning-search
bindkey '\e[B' down-line-or-beginning-search

export EDITOR="vim"

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list '+' '+m:{[:lower:]}={[:upper:]}' '+m:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+r:|[._-]=** r:|=** l:|=*'
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' ignored-patterns '*.class'

autoload -Uz compinit
compinit

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

export PATH=~/.bin:$HOME/.dotnet:~/.local/bin:$HOME/.platformio/penv/bin:/usr/local/go/bin:$PATH

fignore=(o)

function SetTtyColors() {
    echo -en "\e]P0""292e2f" #black       1
    echo -en "\e]P1""dc4848" #darkgrey    2
    echo -en "\e]P2""00aa00" #darkred     3
    echo -en "\e]P3""d99f4e" #red         4
    echo -en "\e]P4""4444aa" #darkgreen   5
    echo -en "\e]P5""c436b9" #green       6
    echo -en "\e]P6""00aaaa" #brown       7
    echo -en "\e]P7""d7d7d7" #yellow      8
    echo -en "\e]P8""7b7b7b" #darkblue    9
    echo -en "\e]P9""6f6fff" #blue        10
    echo -en "\e]PA""55ff55" #darkmagenta 11
    echo -en "\e]PB""ffed8b" #magenta     12
    echo -en "\e]PC""5555ff" #darkcyan    13
    echo -en "\e]PD""ff6eff" #cyan        14
    echo -en "\e]PE""55ffff" #lightgray   15
    echo -en "\e]PF""aaaaaa" #white       16
    clear
}

if [ "$TERM" = "linux" ]; then
    SetTtyColors
fi

function pushdot {
    cd ~/.dotfiles &&
    if [ $# -eq 0 ]; then
        git commit -a && git push -u origin master &
    else
        git commit -a -m "$1" && git push -u origin master | /dev/null &
    fi
    cd -
}

function getdot {
    cd ~/.dotfiles &&
    git pull
    cd -
}

if which starship &> /dev/null; then
    eval "$(starship init zsh)"
fi

if test -f ~/.cargo/env; then
    source ~/.cargo/env
fi

source ~/.shell_aliases
source ~/.zsh_local

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

test -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh && source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
test -f "$HOME/.asdf/asdf.sh" && . "$HOME/.asdf/asdf.sh"
