#!/bin/bash

if [[ "$1" == "" ]]; then
  echo "SUSPEND"
else
  systemctl suspend
fi
