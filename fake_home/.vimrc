" \  / o  _ _
"  \/  | | | |

set nocp
set shell=/usr/bin/zsh

set tsr+=~/.vim/thesaurus/thesaurus-vim-en
set dict+=/usr/share/dict/words

set tags=./tags;/ " Search up to / for tags file
set updatetime=100
set clipboard=unnamedplus " Use system clipboard

if empty($DISPLAY) && empty($SSH_CONNECTION)
    colorscheme elflord
else
    set termguicolors
    let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
    let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
    syntax enable

    colorscheme monokai
endif

" let mapleader=" "
"nnoremap <Leader>w yiwq:PIe `~/.shell_aliases ws <Esc>
"nnoremap <Leader>w yiw:let srr_back = &srr <BAR> set srr=><CR>q:PIr! /home/sage/.shell_aliases wf <Esc>
" TODO Check if file needs saving
" TODO Set and unset srr from within map
set srr=>
nnoremap <Leader>w yiw:w<CR>q:PIr! /home/sage/.shell_aliases wf <CR>yyuq:PIe <ESC><CR>
nnoremap <Leader>s yiw:w<CR>q:PIr! /home/sage/.shell_aliases wf <CR>yyuq:PIvsp <ESC><CR>

" " Auto open NERDTree but focus editing window
" autocmd VimEnter * NERDTree
" autocmd BufEnter * NERDTreeMirror
" autocmd VimEnter * wincmd w
" autocmd VimEnter * NERDTreeFind
" autocmd VimEnter * wincmd w
" " Auto close NERDTree
" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" autocmd VimEnter COMMIT_EDITMSG NERDTreeClose

set mouse=a

set backspace=indent,eol,start

set splitbelow
set splitright

set foldmethod=syntax
set foldlevelstart=99

" Leave some room when scrolling
set scrolloff=3

set conceallevel=0

" Use 4 spaces
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

" Except for makefiles
autocmd FileType make setlocal noexpandtab

" *** BINDINGS ***

" Beautful escapes
inoremap jj <Esc>l
inoremap jk <Esc>l
inoremap kj <Esc>l

" C comment
nnoremap // m`I//<Esc>``:s-////--<Cr>

" Current-file keywords
inoremap <C-J> <C-X><C-N>

" Line numbers
set number relativenumber

set ignorecase smartcase
set autoindent smartindent
"Yank to clipboard
nnoremap <C-C> "+yy
vnoremap <C-C> "+y

" List buffers
nnoremap <Leader>b :ls<CR>:b<Space>

nnoremap <C-I> ^

nnoremap U ~
nnoremap ~ U

" Paste last yank (not delete))
nnoremap "p "0p

" Window jumps
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
tnoremap <C-J> <C-W><C-J>
tnoremap <C-K> <C-W><C-K>
tnoremap <C-L> <C-W><C-L>
tnoremap <C-H> <C-W><C-H>

" Jump to next/prev uppercase/underscore
nnoremap f<cr> /[A-Z_]<cr>
nnoremap F<cr> ?[A-Z_]<cr>

" Map directions to end/beginning of lines
nnoremap yl y$
nnoremap yh y^
nnoremap dl d$
nnoremap dh d^

" <C-P> will replace text after the cursor with unnamed reg contents
nnoremap <C-P> m`v$hp``y$

" Generate and display a printable version
nnoremap <Leader>PP :ha > ~/.vim.ps<CR>:!xdg-open ~/.vim.ps<CR>
vnoremap <Leader>PP :ha > ~/.vim.ps<CR>:!xdg-open ~/.vim.ps<CR>

" Swap current word with next
nnoremap gs "xdiwdwep"xp
" Inverted
nnoremap gb dawbP 

nnoremap <Leader>c :!cat % <Bar> xclip -selection clipboard<CR>

nnoremap <Leader>p :w<CR>:!make run<CR>

nnoremap <Leader>gd :!git diff %<CR>
nnoremap <Leader>gD :!git diff<CR>
nnoremap <Leader>gB :!git blame %<CR>
nnoremap <Leader>gc :!git commit -m ""<Left>
nnoremap <Leader>gh :!git --help<CR>
nnoremap <Leader>gl :!git log<CR>
nnoremap <Leader>gp :!git push<CR>
nnoremap <Leader>gu :!git add -u<CR>
nnoremap <Leader>ga :!git add %<CR>

nnoremap <Leader>r :source ~/.vimrc<CR>
nnoremap <Leader>n :tabedit~/.notes<CR>

nnoremap <Leader>v :vert term ++cols=80<CR>
nnoremap <Leader>t :term ++rows=20<CR>

autocmd BufNewFile,BufRead * if expand('%:t') !~ '\.' | setl spell | endif
autocmd BufNewFile,BufRead *.pbl set syntax=clojure

" Expand %% to the current files dir
cabbr <expr> %% expand('%:p:h')

cnoreabbrev vimc tabedit ~/.vimrc

" Skeletons "
if has ("autocmd")
  augroup templates
    autocmd BufNewFile *.cpp 0r ~/.vim/templates/skeleton.cpp
    autocmd BufNewFile *.c 0r ~/.vim/templates/skeleton.c
    autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
    autocmd BufNewFile *.lisp 0r ~/.vim/templates/skeleton.lisp
    autocmd BufNewFile *.py 0r ~/.vim/templates/skeleton.py
    autocmd BufNewFile *.rb 0r ~/.vim/templates/skeleton.rb
    autocmd BufNewFile makefile 0r ~/.vim/templates/makefile
  augroup END
endif

nnoremap <RightMouse> p
inoremap <RightMouse> <Esc>p

" Spelling corrections
iab ruetn return
iab reteun return
iab reutner return

" Keep same gutter color when errored
" highlight! link SignColumn LineNr

" Include hyphens in words
set iskeyword+=-

so ~/.vim/user/files.vim
